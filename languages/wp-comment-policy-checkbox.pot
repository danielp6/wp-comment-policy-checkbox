# Copyright (C) 2019 Fco. J. Godoy
# This file is distributed under the same license as the WP Comment Policy Checkbox plugin.
msgid ""
msgstr ""
"Project-Id-Version: WP Comment Policy Checkbox 0.3.2\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wp-comment-policy-checkbox\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2019-08-11T10:38:17+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.3.0-alpha-3f55437\n"
"X-Domain: wp-comment-policy-checkbox\n"

#. Plugin Name of the plugin
#: includes/wp-comment-policy-checkbox-data-eraser.php:51
#: includes/wp-comment-policy-checkbox-data-exporter.php:77
msgid "WP Comment Policy Checkbox"
msgstr ""

#. Plugin URI of the plugin
msgid "https://github.com/fcojgodoy/wp-comment-policy-checkbox"
msgstr ""

#. Description of the plugin
msgid "Add a checkbox and custom text to the comment forms so that the user can be informed and give consent to the web's privacy policy. And save this consent in the database."
msgstr ""

#. Author of the plugin
msgid "Fco. J. Godoy"
msgstr ""

#. Author URI of the plugin
msgid "franciscogodoy.es"
msgstr ""

#: wp-comment-policy-checkbox.php:80
msgid "Privacy Policy"
msgstr ""

#. translators: %s: Privacy Policy page link
#: wp-comment-policy-checkbox.php:94
msgid "I have read and accepted the %s"
msgstr ""

#: wp-comment-policy-checkbox.php:125
msgid "WARNING: "
msgstr ""

#: wp-comment-policy-checkbox.php:125
msgid "you must accept the Privacy Policy."
msgstr ""

#: includes/wp-comment-policy-checkbox-data-exporter.php:43
msgid "Comments"
msgstr ""

#: includes/wp-comment-policy-checkbox-admin.php:11
msgid "Comments Policy Checkbox"
msgstr ""

#: includes/wp-comment-policy-checkbox-admin.php:59
msgid "Privacy policy internal page"
msgstr ""

#: includes/wp-comment-policy-checkbox-admin.php:67
msgid "Privacy policy custom page"
msgstr ""

#: includes/wp-comment-policy-checkbox-admin.php:75
msgid "Privacy policy basic information"
msgstr ""

#: includes/wp-comment-policy-checkbox-admin.php:83
msgid "Privacy policy page link types"
msgstr ""

#: includes/wp-comment-policy-checkbox-admin.php:99
msgid "Here you can configure the options for the comment policy checkbox."
msgstr ""

#: includes/wp-comment-policy-checkbox-admin.php:100
msgid "The check box for reading and accepting the privacy policy in the comment forms is active. To deactivate it you must deactivate the WP Comment Policy Check plugin."
msgstr ""

#: includes/wp-comment-policy-checkbox-admin.php:113
msgid "The page with the privacy policy to which you will link the text of the checkbox will be: "
msgstr ""

#: includes/wp-comment-policy-checkbox-admin.php:117
msgid "-- none --"
msgstr ""

#: includes/wp-comment-policy-checkbox-admin.php:146
msgid "This external URL will replace the previous option."
msgstr ""

#: includes/wp-comment-policy-checkbox-admin.php:162
msgid "Write down the basic information about data protection."
msgstr ""

#: includes/wp-comment-policy-checkbox-admin.php:164
msgid "This information will appear above the check box of the privacy policy."
msgstr ""

#: includes/wp-comment-policy-checkbox-admin.php:191
msgid "link types"
msgstr ""

#. translators: 1: 'Link types' link text 2: 'nofollow' code 3: 'Link types' samples
#: includes/wp-comment-policy-checkbox-admin.php:196
msgid "This is for the HTML %1$s attribute of the policy page link. Use space separated words. This can be useful for use %2$s for SEO reasons. Here are some options you could use: %3$s"
msgstr ""
